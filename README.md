[![Release](https://jitpack.io/v/org.traffxml/traff-source-android.svg)](https://jitpack.io/#org.traffxml/traff-source-android)

Javadoc for `master` is at https://traffxml.gitlab.io/traff-source-android/javadoc/master/.

Javadoc for `dev` is at https://traffxml.gitlab.io/traff-source-android/javadoc/dev/.
