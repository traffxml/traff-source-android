/*
 * Copyright © 2017–2020 traffxml.org.
 * 
 * This file is part of the traff-source-android library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.source.android;

import java.util.Collection;

import org.traffxml.traff.TraffMessage;

/* NOTE: 
 * This is a portable class which should be kept clean of any dependencies on platform-specific
 * classes (JRE or Android).
 */

/**
 * Receives updates to the message cache.
 */
public interface MessageListener {
	/**
	 * Processes a message update.
	 * 
	 * <p>The message cache will call this method whenever it receives an update, or when it purges expired
	 * messages from the cache.
	 * 
	 * <p>Any new or updated messages are passed in {@code added}. This argument may be null or empty if this
	 * method is called due to expired messages having been purged.
	 * 
	 * <p>Removed messages are not reported explicitly. Removal of a message may occur for one of the following
	 * reasons: the message may have expired (which can be determined by looking at its expiration time), or
	 * it may have been replaced by a new message (which can be determined by comparing {@code added} to the
	 * set of previously received messages). Cancellations essentially work by replacing the previous message
	 * with a cancellation message. Bear in mind that a message can replace multiple messages, which also
	 * implies that a message can be replaced by one with a different ID.
	 * 
	 * @param added Messages which were newly added to, or updated in, the cache
	 */
	public void onUpdateReceived(Collection<TraffMessage> added);
}
