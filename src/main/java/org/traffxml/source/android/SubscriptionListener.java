/*
 * Copyright © 2017–2020 traffxml.org.
 * 
 * This file is part of the traff-source-android library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.source.android;

import java.util.Map;

/**
 * Receives updates to the message cache which affect one or more subscriptions.
 */
public interface SubscriptionListener {
	/**
	 * Called when a subscription is about to expire.
	 * 
	 * <p>A subscription is about to expire when no activity (setting up the subscription, changing it,
	 * retrieving messages or sending or responding to a heartbeat) has occurred for a significant
	 * period of time. When this event first fires, there is still a significant amount of time left
	 * before the subscription actually expires.
	 * 
	 * <p>The subscriptions are passed in {@code subscriptions}. This is a map whose keys correspond to the
	 * subscription IDs; values correspond to the package name of the subscriber.
	 * 
	 * @param subscriptions The map of subscriptions and subscribers, see description
	 */
	public void onExpiring(Map<String, String> subscriptions);

	/**
	 * Called when the last remaining subscription has expired.
	 * 
	 * <p>Applications can take this as an indicator that the TraFF source is no longer needed, and can take
	 * actions such as shutting down their service, unregistering from upstream sources or releasing a
	 * hardware receiver.
	 */
	public void onLastSubscriptionExpired();

	/**
	 * Processes a message update.
	 * 
	 * <p>The message cache will call this method whenever it receives an update which affects at least one
	 * subscription.
	 * 
	 * <p>The subscriptions are passed in {@code subscriptions}. This is a map whose keys correspond to the
	 * subscription IDs; values correspond to the package name of the subscriber.
	 * 
	 * <p>Message removal is not reported explicitly. Removal of a message may occur for one of the following
	 * reasons: The message may have expired, which the subscriber can be determine simply by looking at the
	 * expiration time of all messages it has received. The message may have been replaced by a new message
	 * (including a cancellation message), in which case the new message would trigger this method. Bear in
	 * mind that a message can replace multiple messages, which also implies that a message can be replaced
	 * by one with a different ID.
	 * 
	 * @param subscriptions The map of subscriptions and subscribers, see description
	 */
	public void onUpdateReceived(Map<String, String> subscriptions);
}
