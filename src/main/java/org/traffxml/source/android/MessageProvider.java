/*
 * Copyright © 2020 traffxml.org.
 * 
 * This file is part of the traff-source-android library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.source.android;

import java.sql.SQLException;

import org.traffxml.transport.android.AndroidTransport;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

/**
 * A content provider through which other applications can retrieve TraFF messages.
 */
public class MessageProvider extends ContentProvider {
	private MessageCache cache = null;

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		return AndroidTransport.MIME_TYPE_TRAFF;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		return null;
	}

	@Override
	public boolean onCreate() {
		try {
			cache = MessageCache.getInstance(this.getContext());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		/* the only allowed column name is AndroidTransport.COLUMN_DATA ("data") */
		for (String column : projection) {
			if (!column.equalsIgnoreCase(AndroidTransport.COLUMN_DATA))
				throw new IllegalArgumentException(String.format("'%s' is not a legal column name", column));
		}
		/* selection is not supported */
		if ((selection != null) && !selection.isEmpty())
			throw new IllegalArgumentException("Specifying a selection is not supported");
		/* sort order is not supported */
		if ((sortOrder != null) && !sortOrder.isEmpty())
			throw new IllegalArgumentException("Specifying a sort order is not supported");
		String id = uri.getPath();
		if (id == null)
			throw new IllegalArgumentException("Subscription ID must not be null");
		if (id.startsWith("/"))
			id = id.substring(1);
		try {
			return cache.pollForCursor(id);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		return 0;
	}
}
