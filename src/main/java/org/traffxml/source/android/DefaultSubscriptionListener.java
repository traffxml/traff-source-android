/*
 * Copyright © 2017–2020 traffxml.org.
 * 
 * This file is part of the traff-source-android library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.source.android;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.transport.android.AndroidTransport;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

/**
 * Default {@link SubscriptionListener} implementation.
 * 
 * <p>This implementation provides default actions for the {@link #onExpiring(Map)} event (sending a
 * heartbeat for each expiring subscription) and for the {@link #onUpdateReceived(Map)} event (sending a
 * push request for each affected subscription).
 * 
 * <p>Subclasses must provide their own implementation for {@link #onLastSubscriptionExpired()}.
 */
public abstract class DefaultSubscriptionListener extends BroadcastReceiver implements SubscriptionListener {
	/**
	 * The logger for log output.
	 * 
	 * <p>Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(DefaultSubscriptionListener.class);

	private final String authority;
	private final Context context;

	/**
	 * @param authority The authority for the content provider from which subscribers can retrieve messages
	 * @param context A context which will be used to send broadcasts. This should be the application context,
	 * not the context of any component which may not exist for the entire lifecycle of the application.
	 */
	public DefaultSubscriptionListener(String authority, Context context) {
		super();
		this.authority = authority;
		this.context = context;
	}

	/**
	 * Called when a result for a heartbeat request is received.
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent == null)
			return;
		if (!intent.getAction().equals(AndroidTransport.ACTION_TRAFF_HEARTBEAT))
			return;
		if (this.getResultCode() != AndroidTransport.RESULT_OK)
			return;
		Bundle extras = this.getResultExtras(true);
		if (extras == null)
			return;
		String subscriptionId = extras.getString(AndroidTransport.EXTRA_SUBSCRIPTION_ID);
		if (subscriptionId == null)
			return;
		LOG.debug("Received reply to heartbeat for {}", subscriptionId);
		try {
			MessageCache cache = MessageCache.getInstance(context.getApplicationContext());
			cache.touchSubscription(subscriptionId);
		} catch (Exception e) {
			LOG.debug("Failed to update last activity timestamp for " + subscriptionId, e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <p>This implementation sends a heartbeat for each expiring subscription. Upon receiving a result with
	 * a result code of {@link AndroidTransport#RESULT_OK}, it updates the last activity timestamp for that
	 * subscription.
	 */
	@Override
	public void onExpiring(Map<String, String> subscriptions) {
		for (Map.Entry<String, String> entry : subscriptions.entrySet()) {
			Intent outIntent = new Intent(AndroidTransport.ACTION_TRAFF_HEARTBEAT);
			outIntent.setPackage(entry.getValue());
			outIntent.putExtra(AndroidTransport.EXTRA_SUBSCRIPTION_ID, entry.getKey());
			context.sendOrderedBroadcast(outIntent,
					Manifest.permission.ACCESS_COARSE_LOCATION,
					this,
					null, // scheduler,
					AndroidTransport.RESULT_INTERNAL_ERROR, // initialCode,
					null, // initialData,
					null);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <p>This implementation sends a directed
	 * {@link org.traffxml.transport.android.AndroidTransport#ACTION_TRAFF_PUSH}
	 * intent to all affected subscribers, using the subscription ID and package name supplied by the caller.
	 * The data is a URI with a schema of {@code content://}, the authority passed to the constructor and a path
	 * matching the subscription ID.
	 */
	@Override
	public void onUpdateReceived(Map<String, String> subscriptions) {
		for (Map.Entry<String, String> entry : subscriptions.entrySet()) {
			Intent outIntent = new Intent(AndroidTransport.ACTION_TRAFF_PUSH);
			outIntent.setPackage(entry.getValue());
			outIntent.putExtra(AndroidTransport.EXTRA_SUBSCRIPTION_ID, entry.getKey());
			outIntent.setData(Uri.parse(AndroidTransport.CONTENT_SCHEMA + "://" + authority + "/" + entry.getKey()));
			context.sendBroadcast(outIntent, Manifest.permission.ACCESS_COARSE_LOCATION);
		}
	}
}
