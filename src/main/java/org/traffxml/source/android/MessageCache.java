/*
 * Copyright © 2017–2020 traffxml.org.
 * 
 * This file is part of the traff-source-android library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.source.android;

import java.lang.ref.WeakReference;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentSkipListMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.traff.BoundingBox;
import org.traffxml.traff.TraffFeed;
import org.traffxml.traff.TraffMessage;
import org.traffxml.traff.subscription.FilterItem;
import org.traffxml.traff.subscription.Subscription;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

/* NOTE: 
 * Since Android does not play nicely with JDBC, we need an Android-specific implementation here…
 */

/**
 * Handles storage, retrieval and expiration of TraFF messages.
 * 
 * <p>This class is a weak singleton, which has the following implications:
 * <ul>
 * <li>This class is never instantiated directly. Rather, an instance is obtained by calling
 * {@link #getInstance(Context)}.</li>
 * <li>At any given time, there can never be more than one instance of this class.</li>
 * <li>The instance may be garbage-collected after the last reference to it is deleted. In that
 * case, the next call to {@link #getInstance(Context)} will create a new instance.</li>
 * </ul>
 * 
 * <p>The message cache can be configured by calling {@link #setProperties(Properties)} before an
 * instance is obtained. Properties not starting with {@code cache.} are ignored, thus the
 * properties can be shared with other components as long as the {@code cache.} prefix is reserved
 * for the message cache.
 */
public final class MessageCache {
	/**
	 * The logger for log output.
	 * 
	 * <p>Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(MessageCache.class);

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	/** 
	 * SQL statements to initialize the database for persistent storage of messages.
	 */
	private static final String[] DB_INIT_STMTS = {
			// Drop existing tables and indices
			"drop index if exists filter_index;",
			"drop index if exists message_expires_index;",
			"drop index if exists message_bbox_index;",
			"drop index if exists message_roadClass_index;",
			"drop index if exists message_firstFeed_index;",
			"drop table if exists filter;",
			"drop table if exists subscription;",
			"drop table if exists message;",
			"drop table if exists feed;",
			// Subscription
			"create table subscription(id varchar(255) primary key not null, packageName varchar(255) not null, lastFeed bigint default 0 not null, lastActivity timestamp(0));",
			// Filter
			"create table filter(subscriptionId varchar(255) not null, minRoadClass varchar(16), minLat float, minLon float, maxLat float, maxLon float, foreign key(subscriptionId) references subscription(id) on delete cascade);",
			"create index filter_index on filter(subscriptionId, minRoadClass, minLat, minLon, maxLat, maxLon);",
			// Message
			"create table message(id varchar(255) primary key not null, expires timestamp(0) not null, roadClass varchar(16), minLat float not null, minLon float not null, maxLat float not null, maxLon float not null, firstFeed bigint not null, data clob not null);",
			"create index message_expires_index on message(expires);",
			"create index message_bbox_index on message(minLat, minLon, maxLat, maxLon);",
			"create index message_roadClass_index on message(roadClass);",
			"create index message_firstFeed_index on message(firstFeed);",
			// Feed
			"create table feed(feed bigint not null);",
			"insert into feed(feed) values(1);",
	};

	private static final String DB_TABLE_FILTER = "filter";

	private static final String DB_TABLE_MESSAGE = "message";

	private static final String DB_TABLE_SUBSCRIPTION = "subscription";

	/** 
	 * DB schema version for persistent storage of cached messages, incremented every time the schema changes.
	 * 
	 * <p>You may wish to store this value in (or along with) your database and compare it on startup.
	 * If the versions do not match, reinitialize your local database. 
	 */
	private static final int DB_VERSION_MESSAGE_CACHE = 2;

	/** Milliseconds per minute. */
	private static final int MILLIS_PER_MINUTE = 60000;

	/**
	 * Shared query portion for {@link #POLL_QUERY_STMT} and {@link #SUBSCRIPTIONS_AFFECTED_STMT}.
	 */
	private static final String SUBSCRIPTION_MATCH_SUBSTMT =
			"filter.subscriptionId = subscription.id\n" + 
					"  and message.firstFeed > subscription.lastFeed\n" + 
					"  and ( -- road class matches\n" + 
					"    filter.minRoadClass is null\n" + 
					"    or filter.minRoadClass = 'OTHER'\n" + 
					"    or message.roadClass is null\n" + 
					"    or filter.minRoadClass = message.roadClass\n" + 
					"    or (filter.minRoadClass = 'TRUNK' and message.roadClass = 'MOTORWAY')\n" + 
					"    or (filter.minRoadClass = 'PRIMARY' and message.roadClass in ('MOTORWAY', 'TRUNK'))\n" + 
					"    or (filter.minRoadClass = 'SECONDARY' and message.roadClass in ('MOTORWAY', 'TRUNK', 'PRIMARY'))\n" + 
					"    or (filter.minRoadClass = 'TERTIARY' and message.roadClass in ('MOTORWAY', 'TRUNK', 'PRIMARY', 'SECONDARY'))\n" + 
					"  )\n" + 
					"  and ( -- bbox matches\n" + 
					"    filter.minLat is null\n" + 
					"    -- bboxes share a pole\n" + 
					"    or (filter.minLat = -90 and message.minLat = -90)\n" + 
					"    or (filter.maxLat = 90 and message.maxLat = 90)\n" + 
					"    or ( -- general case: latitude ranges must overlap\n" + 
					"      (not (filter.minLat > message.maxLat or filter.maxLat < message.minLat))\n" + 
					"      and ( -- examine only longitudes\n" + 
					"        -- bboxes share the 180 degree meridian\n" + 
					"        ((abs(filter.minLon) = 180 or abs(filter.maxLon) = 180) and (abs(message.minLon) = 180 or abs(message.maxLon) = 180))\n" + 
					"        -- neither bbox crosses the 180 degree meridian\n" + 
					"        or (filter.minLon <= filter.maxLon\n" + 
					"          and message.minLon <= message.maxLon\n" + 
					"          and not (filter.minLon > message.maxLon or filter.maxLon < message.minLon)\n" + 
					"        )\n" + 
					"        -- both bboxes cross (and thus share) the 180 degree meridian\n" + 
					"        or (filter.minLon > filter.maxLon and message.minLon > message.maxLon)\n" + 
					"        -- filter crosses the 180 degree meridian but message does not\n" + 
					"        or (filter.minLon > filter.maxLon\n" + 
					"          and message.minLon <= message.maxLon\n" + 
					"          and not (filter.minLon > message.maxLon and filter.maxLon < message.minLon)\n" + 
					"        )\n" + 
					"        -- filter does not cross the 180 degree meridian but message does\n" + 
					"        or (filter.minLon <= filter.maxLon\n" + 
					"          and message.minLon > message.maxLon\n" + 
					"          and not (filter.minLon > message.maxLon and filter.maxLon < message.minLon)\n" + 
					"        )\n" + 
					"      )\n" + 
					"    )\n" + 
					"  );";

	/**
	 * SQL statement for poll queries.
	 */
	private static final String POLL_QUERY_STMT = 
			"select distinct message.*\n" +
					"from message, filter, subscription\n" + 
					"where subscription.id = ?\n" + 
					"  and " + 
					SUBSCRIPTION_MATCH_SUBSTMT;

	/**
	 * SQL statement to determine subscriptions with new messages.
	 */
	private static final String SUBSCRIPTIONS_AFFECTED_STMT = 
			"select distinct subscription.id, subscription.packageName\n" +
					"from message, filter, subscription\n" + 
					"where " + 
					SUBSCRIPTION_MATCH_SUBSTMT;

	private static Context context;

	private static WeakReference<MessageCache> instance;

	/** The timer to remove expired messages, if automatic purging is enabled. */
	private Timer messageExpirationTimer = null;

	/** The timer to remove expired subscriptions, if a timeout is set. */
	private Timer subscriptionExpirationTimer = null;

	/**
	 * Listeners which get notified whenever new messages are added or removed.
	 */
	private List<WeakReference<MessageListener>> listeners = Collections.synchronizedList(new ArrayList<WeakReference<MessageListener>>());

	/**
	 * Listeners which get notified whenever new messages are available for a subscription.
	 */
	private List<WeakReference<SubscriptionListener>> subscriptionListeners =
			Collections.synchronizedList(new ArrayList<WeakReference<SubscriptionListener>>());

	/**
	 * Timeout for inactive subscriptions in seconds.
	 */
	private Integer subscriptionTimeout = null;

	/**
	 * In-memory copy of the message cache database.
	 * 
	 * <p>This is a concurrent-access map, i.e. simple access operations are thread-safe. However, successive
	 * access operations which may leave the map in an inconsistent state, as well as iterations, must be
	 * explicitly synchronized to the map. Allocations of this field (which should happen only once) must
	 * occur in code blocks synchronized to the MessageCache instance.
	 */
	private Map<String, TraffMessage> cachedMessages = null;

	/**
	 * Properties for the message cache.
	 */
	private static Properties properties = new Properties();

	/**
	 * Whether to cache messages in memory for better performance.
	 */
	private boolean twoLevelCache = true;

	/** The database. */
	private SQLiteDatabase db;

	private DbHelper dbHelper;

	/**
	 * Obtains an instance of the MessageCache.
	 * 
	 * @return The instance
	 * 
	 * @throws SQLException If any of the database operations fails
	 * @throws ClassNotFoundException If the database driver cannot be found
	 */
	public static synchronized MessageCache getInstance(Context context) throws ClassNotFoundException, SQLException {
		MessageCache strongInstance = null;

		if (instance != null) {
			strongInstance = instance.get();
			if (strongInstance != null)
				return strongInstance;
		}

		// need to (re-)instantiate
		MessageCache.context = context.getApplicationContext();
		strongInstance = new MessageCache();
		instance = new WeakReference<MessageCache>(strongInstance);
		return strongInstance;
	}

	/**
	 * Sets configuration properties for the message cache.
	 * 
	 * <p>This method must be called before the first call to {@link #getInstance(Context)}, else an
	 * {@link IllegalStateException} will be thrown.
	 * 
	 * @param props The properties to set.
	 */
	public static synchronized void setProperties(Properties props) {
		/* find out if we are instantiated, and throw an exception if we are */
		MessageCache strongInstance = null;

		if (instance != null) {
			strongInstance = instance.get();
			if (strongInstance != null)
				throw new IllegalStateException("Properties cannot be set once an instance has been created");
		}

		properties = props;
	}

	/**
	 * Registers a new {@link MessageListener}
	 * 
	 * <p>Listeners will receive notifications for any messages added or removed through this {@code MessageCache}
	 * instance. If the {@code MessageCache} is backed by a shared database, listeners will not get notified
	 * about any additions or deletions which bypass this cache.
	 * 
	 * @param listener The new listener
	 */
	public void addListener(MessageListener listener) {
		boolean isInList = false;
		synchronized(listeners) {
			for (WeakReference<MessageListener> ref : listeners)
				if (ref.get() == listener) {
					isInList = true;
					break;
				}
			if (!isInList)
				listeners.add(new WeakReference<MessageListener>(listener));
		}
	}

	/**
	 * Registers a new {@link SubscriptionListener}
	 * 
	 * <p>Listeners will receive notifications for any messages added or removed through this {@code MessageCache}
	 * instance if they affect at least one subscription. If the {@code MessageCache} is backed by a shared
	 * database, listeners will not get notified about any additions or deletions which bypass this cache.
	 * 
	 * <p>If {@code listener} is already registered, this is a no-op.
	 * 
	 * @param listener The new listener
	 */
	public void addListener(SubscriptionListener listener) {
		boolean isInList = false;
		synchronized(subscriptionListeners) {
			for (WeakReference<SubscriptionListener> ref : subscriptionListeners)
				if (ref.get() == listener) {
					isInList = true;
					break;
				}
			if (!isInList)
				subscriptionListeners.add(new WeakReference<SubscriptionListener>(listener));
		}
	}

	/**
	 * Changes an existing subscription in the database.
	 * 
	 * <p>Changing a subscription means giving it a new filter list, discarding the previous one.
	 * 
	 * @param id The identifier for the subscription to change
	 * @param filterList The new filter list for the subscription
	 * 
	 * @return A {@link Subscription} with the ID assigned by the system and the caller-supplied
	 * filter list
	 * 
	 * @throws IllegalArgumentException if {@code id} does not match a known subscription ID
	 * @throws SQLException If the subscription could not be modified in the database
	 */
	public Subscription changeSubscription(String id, List<FilterItem> filterList)
			throws IllegalArgumentException, SQLException {
		if (!isValidSubscription(id))
			throw new IllegalArgumentException(String.format("%s is not a valid subscription", id));
		db.beginTransaction();
		ContentValues values = new ContentValues();
		values.put("lastFeed", 0);
		values.put("lastActivity", DATE_FORMAT.format(new Date()));
		db.update(DB_TABLE_SUBSCRIPTION, values, "id = ?", new String[] {id});
		db.delete(DB_TABLE_FILTER, "subscriptionId = ?", new String[] {id});
		addFilterList(id, filterList);
		db.setTransactionSuccessful();
		db.endTransaction();
		return new Subscription(id, filterList);
	}

	/**
	 * Returns the timeout for inactive subscriptions in seconds.
	 * 
	 * <p>A null return value indicates that subscriptions never time out.
	 * 
	 * @return the subscriptionTimeout
	 */
	public Integer getSubscriptionTimeout() {
		return subscriptionTimeout;
	}

	/**
	 * Whether this cache has any active subscriptions.
	 * 
	 * @return True if at least one subscription is active, false if not.
	 */
	public boolean hasSubscriptions() {
		Cursor cursor = db.rawQuery("select * from subscription;", null);
		while (cursor.moveToNext()) {
			cursor.close();
			return true;
		}
		cursor.close();
		return false;
	}

	/**
	 * Whether a given ID refers to an existing subscription.
	 * 
	 * @param id A subscription ID
	 * 
	 * @return True if a subscription with that ID exists, false if not.
	 * @throws SQLException 
	 */
	public boolean isValidSubscription(String id) throws SQLException {
		Cursor cursor = db.rawQuery("select * from subscription where id = ?;", new String[]{id});
		boolean res = !cursor.isAfterLast();
		cursor.close();
		return res;
	}

	/**
	 * Polls the cache for new messages matching the subscription.
	 * 
	 * <p>Polling the cache will return all messages matching at least one filter item in the filter
	 * list of the subscription for the first poll after a subscription has been set up or changed.
	 * For subsequent subscriptions, the selection is further limited to messages which have changed
	 * since the last poll operation.
	 * 
	 * <p>Internally, a poll operation first ensures that the {@code feed} counter is bigger than the
	 * highest {@code firstFeed} of any message returned, before any messages are retrieved.
	 * 
	 * <p>After retrieving the messages, the {@code lastFeed} column for the subscription is set to
	 * the highest {@code firstFeed} observed among the messages returned.
	 * 
	 * <p>Maintaining this order is important to prevent race conditions between poll and update
	 * operations taking place concurrently, ensuring no messages are skipped.
	 * 
	 * @param id The subscription identifier
	 * 
	 * @return A feed of all messages matching the criteria
	 * 
	 * @throws IllegalArgumentException if {@code id} does not match a known subscription ID
	 * @throws SQLException
	 */
	public TraffFeed poll(String id)
			throws IllegalArgumentException, SQLException {
		if (!isValidSubscription(id))
			throw new IllegalArgumentException(String.format("%s is not a valid subscription", id));
		db.beginTransaction();
		ContentValues values = new ContentValues();
		values.put("lastActivity", DATE_FORMAT.format(new Date()));
		db.update(DB_TABLE_SUBSCRIPTION, values, "id = ?", new String[] {id});

		db.execSQL(
				"update feed set feed = (select max(firstFeed) + 1 from message) " +
				"where feed <= (select max(firstFeed) from message);");

		long feed = 0;
		List<TraffMessage> outMessages = new ArrayList<TraffMessage>();
		Cursor cursor = db.rawQuery(POLL_QUERY_STMT, new String[] {id});

		/* Synchronize with cachedMessages if we’re going to use them, else sync with local var as a dummy */
		synchronized ((cachedMessages != null) ? cachedMessages : outMessages) {
			while (cursor.moveToNext()) {
				try {
					long firstFeed = cursor.getLong(cursor.getColumnIndex("firstFeed"));
					if (firstFeed > feed)
						feed = firstFeed;
					if (twoLevelCache && (cachedMessages != null))
						/* if we have the message in memory, avoid another costly deserialization */
						outMessages.add(cachedMessages.get(cursor.getString(cursor.getColumnIndex("id"))));
					else
						outMessages.add(TraffMessage.read(cursor.getString(cursor.getColumnIndex("data"))));
				} catch (Exception e) {
					LOG.debug("{}", e);
				}
			}
		}

		cursor.close();

		values = new ContentValues();
		values.put("lastFeed", feed);
		db.update(DB_TABLE_SUBSCRIPTION, values, "id = ?", new String[] {id});

		db.setTransactionSuccessful();
		db.endTransaction();
		return new TraffFeed(outMessages);
	}

	/**
	 * Polls the cache for new messages matching the subscription.
	 * 
	 * <p>This method is intended for use by a content provider. It will return a cursor, which the caller must
	 * close after retrieving the results. The records returned have a {@code data} column, which contains
	 * the message in XML format. If other columns are returned, they are for internal use only and should
	 * not be used by a client, as their availability in future versions is not guaranteed.
	 * 
	 * <p>Polling the cache will return all messages matching at least one filter item in the filter
	 * list of the subscription for the first poll after a subscription has been set up or changed.
	 * For subsequent subscriptions, the selection is further limited to messages which have changed
	 * since the last poll operation.
	 * 
	 * <p>Internally, a poll operation first ensures that the {@code feed} counter is bigger than the
	 * highest {@code firstFeed} of any message returned, before any messages are retrieved.
	 * 
	 * <p>After retrieving the messages, the {@code lastFeed} column for the subscription is set to
	 * the highest {@code firstFeed} observed among the messages returned.
	 * 
	 * <p>Maintaining this order is important to prevent race conditions between poll and update
	 * operations taking place concurrently, ensuring no messages are skipped.
	 * 
	 * @param id The subscription identifier
	 * 
	 * @return A cursor to iterate over the messages
	 * 
	 * @throws IllegalArgumentException if {@code id} does not match a known subscription ID
	 * @throws SQLException
	 */
	public Cursor pollForCursor(String id) throws SQLException {
		if (!isValidSubscription(id))
			throw new IllegalArgumentException(String.format("%s is not a valid subscription", id));
		db.beginTransaction();
		ContentValues values = new ContentValues();
		values.put("lastActivity", DATE_FORMAT.format(new Date()));
		db.update(DB_TABLE_SUBSCRIPTION, values, "id = ?", new String[] {id});

		db.execSQL(
				"update feed set feed = (select max(firstFeed) + 1 from message) " +
				"where feed <= (select max(firstFeed) from message);");

		long feed = 0;
		Cursor cursor = db.rawQuery(POLL_QUERY_STMT, new String[] {id});

		while (cursor.moveToNext()) {
			try {
				long firstFeed = cursor.getLong(cursor.getColumnIndex("firstFeed"));
				if (firstFeed > feed)
					feed = firstFeed;
			} catch (Exception e) {
				LOG.debug("{}", e);
			}
		}
		cursor.moveToPosition(-1);

		values = new ContentValues();
		values.put("lastFeed", feed);
		db.update(DB_TABLE_SUBSCRIPTION, values, "id = ?", new String[] {id});

		db.setTransactionSuccessful();
		db.endTransaction();
		return cursor;
	}

	/**
	 * Processes a collection of new TraFF messages.
	 * 
	 * <p>Messages which have already expired are discarded immediately.
	 * 
	 * <p>Then, for each new message, existing messages which will be overridden are read from the database.
	 * Together with the bounding box of the new message location, they are used to determine the
	 * bounding box for the new message. Overridden messages with a different ID are deleted and the
	 * previous version of the new message is update, or the new message is added to the database if no
	 * previous version exists. The {@code firstFeed} column of the new (or updated) message is set to
	 * the value of {@code feed}.
	 * 
	 * <p>TODO The road class is currently taken from the new message. Values in old messages are ignored.
	 * 
	 * @param newMessages The new message
	 * 
	 * @throws SQLException 
	 */
	public void processUpdate(Collection<TraffMessage> newMessages) throws SQLException {
		/* drop expired messages */
		Date now = new Date();
		Set<TraffMessage> expired = new TreeSet<TraffMessage>();
		for (TraffMessage message : newMessages)
			if (message.isExpired(now))
				expired.add(message);
		newMessages.removeAll(expired);

		db.beginTransaction();

		/* Synchronize with cachedMessages if we’re going to use them, else sync with local var as a dummy */
		synchronized ((cachedMessages != null) ? cachedMessages : expired) {
			for (TraffMessage message : newMessages) {
				if (twoLevelCache && (cachedMessages != null)) {
					/* if we have a cached copy of the messages, update it */
					for (String replacedId : message.replaces)
						if (cachedMessages.containsKey(replacedId))
							cachedMessages.remove(replacedId);
					cachedMessages.put(message.id, message);
				}

				List<String> ids = new ArrayList<String>();
				ids.add(message.id);
				ids.addAll(Arrays.asList(message.replaces));
				String stmtQ = "select * from message where id in (" + TextUtils.join(",", Collections.nCopies(ids.size(), "?")) + ")";
				Cursor cursor = db.rawQuery(stmtQ, ids.toArray(new String[]{}));
				BoundingBox bbox = null;
				if (message.location != null)
					bbox = message.location.getBoundingBox();
				boolean update = false;
				boolean delete = false;
				while (cursor.moveToNext()) {
					if (message.id.equals(cursor.getString(cursor.getColumnIndex("id"))))
						update = true;
					else
						delete = true;
					if (bbox == null)
						bbox = new BoundingBox(cursor.getFloat(cursor.getColumnIndex("minLat")),
								cursor.getFloat(cursor.getColumnIndex("minLon")),
								cursor.getFloat(cursor.getColumnIndex("maxLat")),
								cursor.getFloat(cursor.getColumnIndex("maxLon")));
					else
						bbox = bbox.merge(new BoundingBox(cursor.getFloat(cursor.getColumnIndex("minLat")),
								cursor.getFloat(cursor.getColumnIndex("minLon")),
								cursor.getFloat(cursor.getColumnIndex("maxLat")),
								cursor.getFloat(cursor.getColumnIndex("maxLon"))));
					// TODO road class?
				}
				cursor.close();

				if (delete) {
					String whereClause = "id in (" + TextUtils.join(",", Collections.nCopies(ids.size(), "?")) + ")";
					db.delete(DB_TABLE_MESSAGE, whereClause, message.replaces);
				}

				try {
					ContentValues values = new ContentValues();
					values.put("expires", DATE_FORMAT.format(message.getEffectiveExpirationTime()));
					if ((message.location != null) && (message.location.roadClass != null))
						values.put("roadClass",message.location.roadClass.name());
					values.put("minLat", bbox.minLat);
					values.put("minLon", bbox.minLon);
					values.put("maxLat", bbox.maxLat);
					values.put("maxLon", bbox.maxLon);
					cursor = db.rawQuery("select max(feed) from feed", null);
					if (cursor.moveToFirst())
						values.put("firstFeed", cursor.getInt(0));
					else
						values.put("firstFeed", 1);
					cursor.close();
					values.put("data", message.toXml());

					if (update)
						db.update(DB_TABLE_MESSAGE, values, "id = ?", new String[] {message.id});
					else {
						values.put("id", message.id);
						db.insert(DB_TABLE_MESSAGE, null, values);
					}
				} catch (Exception e) {
					LOG.warn("Failed to update message {} in the database", message.id, e);
				}
			}
		}

		Map<String, String> subscriptions = new HashMap<String, String>();
		Cursor cursor = db.rawQuery(SUBSCRIPTIONS_AFFECTED_STMT, null);
		while (cursor.moveToNext())
			try {
				subscriptions.put(cursor.getString(cursor.getColumnIndex("id")), cursor.getString(cursor.getColumnIndex("packageName")));
			} catch (Exception e) {
				// NOP
			}
		cursor.close();

		if (!subscriptions.isEmpty())
			for (WeakReference<SubscriptionListener> listenerRef : subscriptionListeners) {
				SubscriptionListener listener = listenerRef.get();
				if (listener != null)
					listener.onUpdateReceived(new HashMap<String, String>(subscriptions));
			}

		db.setTransactionSuccessful();
		db.endTransaction();

		notifyListeners(newMessages);
	}

	/**
	 * Purges expired messages from the cache.
	 * 
	 * @return The number of records purged
	 * 
	 * @throws SQLException
	 */
	public int purgeExpiredMessages() throws SQLException {
		Date date = new Date();

		if (twoLevelCache && (cachedMessages != null)) {
			synchronized(cachedMessages) {
				Iterator<Entry<String, TraffMessage>> iter = cachedMessages.entrySet().iterator();
				while (iter.hasNext()) {
					Entry<String, TraffMessage> entry = iter.next();
					if (entry.getValue().getEffectiveExpirationTime().before(date))
						iter.remove();
				}
			}
		}

		String dateString = DATE_FORMAT.format(date);
		int res = db.delete(DB_TABLE_MESSAGE, "expires < ?", new String[] {dateString});
		return res;
	}

	/**
	 * Retrieves a list of messages currently in the cache.
	 * 
	 * <p>If {@code source} is specified, only messages from that source will be retrieved; otherwise
	 * all messages will be returned.
	 * 
	 * <p>The return value is a list of messages, not a feed, as results are intended for internal use.
	 * 
	 * @param source The source identifier, without a trailing separator character
	 * 
	 * @return A list of messages matching the criteria
	 * 
	 * @throws SQLException
	 */
	public synchronized List<TraffMessage> query(String source) throws SQLException {
		List<TraffMessage> res = new ArrayList<TraffMessage>();

		/* Synchronize with cachedMessages if we’re going to use them, else sync with local var as a dummy */
		synchronized((twoLevelCache && (cachedMessages != null)) ? cachedMessages : res) {
			if (twoLevelCache && (cachedMessages != null)) {
				/* respond from cache if we can, avoiding costly deserialization */
				if (source == null)
					res.addAll(cachedMessages.values());
				else
					for (Entry<String, TraffMessage> entry : cachedMessages.entrySet())
						if (entry.getKey().startsWith(source + ":"))
							res.add(entry.getValue());
				return res;
			}

			Cursor cursor;
			if (source == null)
				cursor = db.rawQuery("select * from message", null);
			else
				cursor = db.rawQuery(
						String.format("select * from message where id like '%s:%%'", source), null);
			while (cursor.moveToNext())
				try {
					String id = cursor.getString(cursor.getColumnIndex("id"));
					if (!cachedMessages.containsKey(id))
						res.add(TraffMessage.read(cursor.getString(cursor.getColumnIndex("data"))));
				} catch (Exception e) {
					LOG.warn("Exception when trying to read a message from the database", e);
				}
			cursor.close();

			if (twoLevelCache && (source == null)) {
				/* cache the response */
				Map<String, TraffMessage> cache = new ConcurrentSkipListMap<String, TraffMessage>();
				for (TraffMessage message : res)
					cache.put(message.id, message);
				cachedMessages = cache;
			}
		}

		return res;
	}

	/**
	 * Unregisters a {@link MessageListener}.
	 * 
	 * <p>Unregistering a null listener, or a listener that is not currently registered, is a no-op.
	 * 
	 * <p>This method will also remove from the list any entries pointing to listeners which have been
	 * recycled by the garbage collector.
	 * 
	 * @param listener The new listener
	 */
	public void removeListener(MessageListener listener) {
		List<WeakReference<MessageListener>> toRemove = new ArrayList<WeakReference<MessageListener>>();
		synchronized(listeners) {
			for (WeakReference<MessageListener> ref : listeners)
				if ((ref.get() == listener) || (ref.get() == null))
					toRemove.add(ref);
		}
		for (WeakReference<MessageListener> ref : toRemove)
			listeners.remove(ref);
	}

	/**
	 * Unregisters a {@link SubscriptionListener}.
	 * 
	 * <p>Unregistering a null listener, or a listener that is not currently registered, is a no-op.
	 * 
	 * <p>This method will also remove from the list any entries pointing to listeners which have been
	 * recycled by the garbage collector.
	 * 
	 * @param listener The new listener
	 */
	public void removeListener(SubscriptionListener listener) {
		List<WeakReference<SubscriptionListener>> toRemove = new ArrayList<WeakReference<SubscriptionListener>>();
		synchronized(subscriptionListeners) {
			for (WeakReference<SubscriptionListener> ref : subscriptionListeners)
				if ((ref.get() == listener) || (ref.get() == null))
					toRemove.add(ref);
		}
		for (WeakReference<SubscriptionListener> ref : toRemove)
			subscriptionListeners.remove(ref);
	}

	/**
	 * Switches automatic message purging on or off.
	 * 
	 * <p>If automatic purging is enabled, a timer task will remove expired messages once per minute.
	 * 
	 * <p>If the cache is backed by a shared database, only one MessageCache instance needs to have automatic
	 * purging enabled. Having multiple auto-purging cache instances burns a few unnecessary CPU cycles each
	 * minute but will not do any further harm.
	 * 
	 * @param autoPurgeMessages True to enable automatic message purging, false to disable it
	 */
	public void setAutoPurgeMessages(boolean autoPurgeMessages) {
		if (autoPurgeMessages && (messageExpirationTimer == null)) {
			messageExpirationTimer = new Timer("messageExpirationTimer", true);
			messageExpirationTimer.schedule(new MessageExpirationTimerTask(), 0, MILLIS_PER_MINUTE);
		} else if (!autoPurgeMessages && (messageExpirationTimer != null)) {
			messageExpirationTimer.cancel();
			messageExpirationTimer = null;
		}
	}

	/**
	 * Sets the duration for which a subscription can be inactive before it times out.
	 * 
	 * <p>Setting the timeout to null, or a value of zero or less, will prevent subscriptions from
	 * timing out.
	 * 
	 * <p>This method currently does not take existing subscriptions into account, and any changes will
	 * affect them immediately. If the source has announced its timeout duration or lack of a timeout to its
	 * consumers (which is optional on Android), introducing a new timeout or shortening an existing timeout
	 * would result in a violation of the subscription contract as subscriptions would time out unexpectedly.
	 * It is recommended that sources take measures to prevent this situation: either do not communicate a
	 * timeout (or lack of one) to consumers, or refrain from tightening timeout values while there are
	 * active subscriptions.
	 * 
	 * @param subscriptionTimeout the subscriptionTimeout to set
	 */
	public void setSubscriptionTimeout(Integer subscriptionTimeout) {
		/* TODO should we restrict this operation when there are active subscriptions? */
		Integer newTimeout = subscriptionTimeout;
		if ((newTimeout != null) && (newTimeout <= 0))
			newTimeout = null;
		LOG.debug("setting subscription timeout to {}", newTimeout);
		if ((newTimeout == null) && (this.subscriptionTimeout != null) && (subscriptionExpirationTimer != null)) {
			LOG.debug("canceling existing subscriptionExpirationTimer");
			subscriptionExpirationTimer.cancel();
			subscriptionExpirationTimer = null;
		} else if ((newTimeout != null) && (this.subscriptionTimeout == null)) {
			// TODO currently set to once per minute, should we adjust that based on the actual timeout?
			if (subscriptionExpirationTimer == null) {
				LOG.debug("creating new subscriptionExpirationTimer");
				subscriptionExpirationTimer = new Timer("subscriptionExpirationTimer", true);
				subscriptionExpirationTimer.schedule(new SubscriptionExpirationTimerTask(), 0, MILLIS_PER_MINUTE);
			}
		} else if ((newTimeout != null) && (this.subscriptionTimeout != null)) {
			/* If we decide to adjust the timer interval based on the timeout, we can do that here. */
		}
		this.subscriptionTimeout = newTimeout;
	}

	/**
	 * Returns the number of messages currently in the cache.
	 */
	public synchronized int size() {
		if (twoLevelCache && (cachedMessages != null))
			return cachedMessages.size();

		int res = 0;
		Cursor cursor;
		cursor = db.rawQuery("select count(*) from message", null);
		while (cursor.moveToNext())
			res = cursor.getInt(0);
		cursor.close();

		if (twoLevelCache && (res == 0))
			cachedMessages = new ConcurrentSkipListMap<String, TraffMessage>();

		return res;
	}

	/**
	 * Adds a new subscription to the database.
	 * 
	 * @param filterList The filter list for the new subscription
	 * 
	 * @return A {@link Subscription} with the ID assigned by the system and the caller-supplied
	 * filter list
	 * 
	 * @throws SQLException If the subscription could not be entered into the database
	 */
	public Subscription subscribe(String packageName, List<FilterItem> filterList) throws SQLException {
		/* Generate a random 128-bit ID using a secure random generator */
		SecureRandom random = new SecureRandom();
		byte idBytes[] = new byte[16];
		random.nextBytes(idBytes);
		String id = "";
		for (byte idByte : idBytes)
			id = String.format("%s%02x", id, idByte);

		ContentValues values = new ContentValues();
		values.put("id", id);
		values.put("packageName", packageName);
		values.put("lastActivity", DATE_FORMAT.format(new Date()));
		db.beginTransaction();
		db.insert(DB_TABLE_SUBSCRIPTION, null, values);
		addFilterList(id, filterList);
		db.setTransactionSuccessful();
		db.endTransaction();
		return new Subscription(id, filterList);
	}

	/**
	 * Updates the last activity timestamp of a subscription to prevent it from expiring.
	 * 
	 * @param id The identifier for the subscription to change
	 * 
	 * @throws IllegalArgumentException if {@code id} does not match a known subscription ID
	 * @throws SQLException If the subscription could not be modified in the database
	 */
	public void touchSubscription(String id)
			throws IllegalArgumentException, SQLException {
		if (!isValidSubscription(id))
			throw new IllegalArgumentException(String.format("%s is not a valid subscription", id));
		db.beginTransaction();
		ContentValues values = new ContentValues();
		values.put("lastActivity", DATE_FORMAT.format(new Date()));
		db.update(DB_TABLE_SUBSCRIPTION, values, "id = ?", new String[] {id});
		db.setTransactionSuccessful();
		db.endTransaction();
		return;
	}

	/**
	 * Removes a subscription from the database.
	 * 
	 * @param id The identifier for the subscription to remove
	 * 
	 * @throws SQLException If the subscription could not be removed (including cases in which the
	 * id does not refer to an existing subscription)
	 */
	public void unsubscribe(String id) throws SQLException {
		db.beginTransaction();
		db.delete(DB_TABLE_SUBSCRIPTION, "id = ?", new String[] {id});
		db.setTransactionSuccessful();
		db.endTransaction();
	}

	protected void finalize() throws Throwable {
		setAutoPurgeMessages(false);
		setSubscriptionTimeout(null);
		if (dbHelper != null)
			dbHelper.close();
		super.finalize();
	}

	/**
	 * Instantiates a new MessageCache.
	 * 
	 * <p>This constructor is not intended to be called directly. Instead, obtain an instance by
	 * calling {@link #getInstance()}.
	 * 
	 * @throws ClassNotFoundException if the JDBC driver is not found
	 * @throws SQLException if the database connection, or initialization, fails
	 */
	private MessageCache() throws SQLException, ClassNotFoundException {
		if (context == null)
			throw new IllegalStateException("Context must be set before obtaining an instance.");
		dbHelper = new DbHelper(context);
		db = dbHelper.getWritableDatabase();
		db.setForeignKeyConstraintsEnabled(true);
	}

	/**
	 * Adds the filter list for a new or changed subscription.
	 * 
	 * <p>If the subscription already exists, the caller is responsible for deleting any existing subscriptions.
	 * 
	 * @param subscriptionId The identifier for the subscription
	 * @param filterList The new filter list
	 * 
	 * @throws SQLException
	 */
	private void addFilterList(String subscriptionId, List<FilterItem> filterList) throws SQLException {
		for (FilterItem item : filterList) {
			ContentValues values = new ContentValues();
			values.put("subscriptionID", subscriptionId);
			if (item.minRoadClass != null)
				values.put("minRoadClass", item.minRoadClass.name());
			if (item.bbox != null) {
				values.put("minLat", item.bbox.minLat);
				values.put("minLon", item.bbox.minLon);
				values.put("maxLat", item.bbox.maxLat);
				values.put("maxLon", item.bbox.maxLon);
			}
			db.insert(DB_TABLE_FILTER, null, values);
		}
	}

	/**
	 * Notifies all registered listeners about changes to the message cache.
	 * 
	 * @param added A newly added or updated message
	 * @param removed Superseded, canceled or expired messages
	 */
	private void notifyListeners(Collection<TraffMessage> added) {
		synchronized(listeners) {
			for (WeakReference<MessageListener> ref : listeners) {
				MessageListener listener = ref.get();
				if (listener != null)
					listener.onUpdateReceived(added);
			}
		}
	}

	private class MessageExpirationTimerTask extends TimerTask {
		public void run() {
			LOG.debug("Purging expired messages");
			try {
				int expired = purgeExpiredMessages();
				if (expired > 0)
					notifyListeners(null);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				LOG.debug("{}", e);
			}
		}
	}

	private class SubscriptionExpirationTimerTask extends TimerTask {
		public void run() {
			LOG.debug("Scanning for expiring and expired subscriptions");
			if ((subscriptionTimeout == null) || (subscriptionTimeout <= 0))
				return;
			Date now = new Date();

			/* delete subscriptions which have timed out */
			Date date = new Date(now.getTime() - (subscriptionTimeout * 1000));
			String dateString = DATE_FORMAT.format(date);
			int deletedSubs = db.delete(DB_TABLE_SUBSCRIPTION, "lastActivity < ?", new String[] {dateString});

			/*
			 * if we have deleted the last subscription (i.e. number of deleted subscriptions is nonzero and
			 * now we have none left), notify listeners and skip the rest
			 */
			if (deletedSubs > 0) {
				LOG.debug("{} expired subscriptions deleted");
				if(!hasSubscriptions()) {
					for (WeakReference<SubscriptionListener> listenerRef : subscriptionListeners) {
						SubscriptionListener listener = listenerRef.get();
						if (listener != null)
							listener.onLastSubscriptionExpired();
					}
					return;
				}
			}

			/*
			 * retrieve subscriptions which are about to time out (lastActivity older than half the timeout)
			 * and notify listeners
			 */
			date = new Date(now.getTime() - (subscriptionTimeout * 500));
			dateString = DATE_FORMAT.format(date);
			Map<String, String> subscriptions = new HashMap<String, String>();
			Cursor cursor = db.rawQuery("select * from subscription where lastActivity < ?", new String[] {dateString});
			while (cursor.moveToNext())
				try {
					subscriptions.put(cursor.getString(cursor.getColumnIndex("id")), cursor.getString(cursor.getColumnIndex("packageName")));
					LOG.debug("Subscription {} from {} has not had any recent activity, notifying listeners",
							cursor.getString(cursor.getColumnIndex("id")),
							cursor.getString(cursor.getColumnIndex("packageName")));
				} catch (Exception e) {
					// NOP
					LOG.warn("Failed to retrieve subscription from database", e);
				}
			cursor.close();
			for (WeakReference<SubscriptionListener> listenerRef : subscriptionListeners) {
				SubscriptionListener listener = listenerRef.get();
				if (listener != null)
					listener.onExpiring(new HashMap<String, String>(subscriptions));
			}
		}
	}

	public class DbHelper extends SQLiteOpenHelper {
		public static final String DB_NAME = "messagecache";

		public DbHelper(Context context) {
			super(context, DB_NAME, null, DB_VERSION_MESSAGE_CACHE);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.rawQuery("pragma secure_delete = false;", null);
			for (String stmtRaw: DB_INIT_STMTS)
				db.execSQL(stmtRaw);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			/*
			 * In order to keep this code universal and handle any upgrade situation, this switch
			 * statement is arranged as follows:
			 * * There is one case for each legacy version.
			 * * Case blocks are in ascending order.
			 * * Each case block upgrades that version to the next higher one.
			 * * Each case block, except for the last one, falls through to the next, in order to
			 *   perform the next upgrade. This means that only the last case block ends with a
			 *   break statement, while the others do not.
			 * * If no case block matches, the default block is executed, which wipes the database
			 *   and recreates it from scratch.
			 */
			switch(oldVersion) {
			case 1:
				/*
				 * 1 to 2: add lastActivity column in subscription table and populate it with current time
				 */
				db.execSQL("alter table subscription add column lastActivity timestamp(0)");
				db.execSQL("update subscription set lastActivity = CURRENT_TIMESTAMP where lastActivity is null;");
				/* keep this break statement at the end of the last case */
				break;
			default:
				/* if noting else works, wipe the database and recreate it */
				onCreate(db);
			}
		}
	}
}
